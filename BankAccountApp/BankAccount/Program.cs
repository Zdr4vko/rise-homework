using System;
using BankAccountNS;

namespace ProgramNS
{
    /// <summary>
    /// Bank account app startup point.
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            BankAccountNS.BankAccount ba = new BankAccount("Mr. Bryan Walton", 11.99);

            ba.Credit(5.77);
            ba.Debit(11.22);
            Console.WriteLine("Current balance is ${0}", ba.Balance);
        }
    }
}
