using NUnitLite;

namespace MyProject.Tests
{
    public class TestRunner
    {
        static int Main(string[] args)
        {
            return new AutoRun().Execute(args);
        }
    }
}